import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styles: []
})
export class LoginComponent implements OnInit {

    form: FormGroup
    get f() { return this.form.controls }

    get emailValidationMessage() {
        return 'Error'
    }

    get passwordValidationMessage() {
        return 'Oups'
    }

    constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) { }

    ngOnInit() {
        this.form = this.createForm()
    }

    createForm(): FormGroup {
        return this.fb.group({
            email: ['', Validators.email],
            password: ['', Validators.compose([Validators.required, Validators.minLength(8)])]
        });
    }

    submit() {
        const email = this.f.email.value
        const password = this.f.password.value

        this.auth.login(email, password).then(x => {
            this.router.navigate(['']);
        }).catch(err => {
            alert('Erreur de ceonnexion')
            console.log(`Erreur lors de la connexion: ${JSON.stringify(err, null, 2)}`)
        })
    }
}
