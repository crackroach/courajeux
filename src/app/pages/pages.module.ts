import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { LoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppLayoutComponent } from './layouts/app-layout/app-layout.component';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';

@NgModule({
    declarations: [LoginComponent, AppLayoutComponent, PasswordRecoveryComponent],
    imports: [
        CommonModule,
        PagesRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class PagesModule { }
