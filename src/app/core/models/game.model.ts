export class Game {
    name: string
    codebar: string
    price: number

    constructor(init?: Partial<Game>) {
        Object.assign(this, init);
    }
}
