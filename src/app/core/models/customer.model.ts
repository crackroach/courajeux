import { Address, Purchase } from '.'

export class Customer {
    id: string
    firstName: string
    lastName: string
    get fullname(): string { return `${this.firstName} ${this.lastName}` }
    email: string
    address: Address
    purchases: Purchase[]

    constructor(init?: Partial<Customer>) {
        Object.assign(this, init)
    }
}
