export class Address {
    civicNumber: string
    streetName: string
    postalCode: string
    city: string
    province: string

    constructor(init?: Partial<Address>) {
        Object.assign(this, init)
    }
}
