import { Game } from "./game.model";

export class Purchase {
    createdAt: string
    games: Game[]
    get total(): number {
        return this.games.reduce((p: Game, c: Game) => {
            if (!p)
                p = new Game({ price: 0 });

            p.price += c.price
            return p
        }).price
    }

    constructor(init?: Partial<Purchase>) {
        Object.assign(this, init);
    }
}
