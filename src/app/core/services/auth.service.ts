import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app'
import { first } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    user$: Observable<firebase.User>

    constructor(private af: AngularFireAuth) {
        this.user$ = af.authState
    }

    login(email: string, password: string) {

        return new Promise<any>((resolve, reject) => {
            this.af.auth.signInWithEmailAndPassword(email, password)
                .then(x => {
                    console.log('Logged in')
                    resolve(x)
                })
                .catch(err => {
                    console.log('Sign in failed')
                    reject(err)
                })
        })
    }

    resetPassword(email: string) {
        return this.af.auth.sendPasswordResetEmail(email)
            .then(x => {
                console.log('Email sent')
            })
            .catch(x => {
                console.log('error sending email')
            })
    }

    logout() {
        this.af.auth.signOut();
    }
}
