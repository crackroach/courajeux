import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlSegment, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, first } from "rxjs/operators";
import { AuthService } from '../services/auth.service';
import { Route } from '@angular/compiler/src/core';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

    constructor(private auth: AuthService, private router: Router) { }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.auth.user$.pipe(map(x => {
            const authenticated = (x && x.emailVerified)
            if (authenticated)
                return true
            this.router.navigate(['/Login'])
            return false
        }));
    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        return this.auth.user$
            .pipe(first(),
                map(x => {
                    const authenticated = (x && x.emailVerified)
                    if (authenticated)
                        return true
                    this.router.navigate(['/Login'])
                    return false
                }))
    }
}
