import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './partials';
import { MDBModule } from './mdbmodule.module';
import { FirebaseModule } from './firebase.module';
import { SingleCardLayoutComponent } from './partials/single-card-layout/single-card-layout.component';

@NgModule({
    declarations: [HeaderComponent, SingleCardLayoutComponent],
    imports: [
        CommonModule,
        MDBModule,
        FirebaseModule
    ],
    exports: [
        MDBModule,
        FirebaseModule,

        HeaderComponent,

        SingleCardLayoutComponent
    ]
})
export class SharedModule { }
