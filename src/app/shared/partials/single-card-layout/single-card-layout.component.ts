import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-single-card-layout',
    templateUrl: './single-card-layout.component.html',
    styles: []
})
export class SingleCardLayoutComponent {


    @Input()
    title: string

}
