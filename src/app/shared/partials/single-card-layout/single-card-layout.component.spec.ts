import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleCardLayoutComponent } from './single-card-layout.component';

describe('SingleCardLayoutComponent', () => {
  let component: SingleCardLayoutComponent;
  let fixture: ComponentFixture<SingleCardLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleCardLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleCardLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
