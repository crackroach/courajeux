import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsModule, DropdownModule, InputsModule, NavbarModule, TableModule, WavesModule, CardsFreeModule, TooltipModule, ModalModule, CheckboxModule, BreadcrumbModule, IconsModule, InputUtilitiesModule } from "angular-bootstrap-md";

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        WavesModule.forRoot(),
        NavbarModule,
        ButtonsModule,
        DropdownModule.forRoot(),
        TableModule,
        CardsFreeModule,
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        CheckboxModule,
        BreadcrumbModule,
        IconsModule,
        InputsModule,
        InputUtilitiesModule
    ],
    exports: [
        WavesModule,
        NavbarModule,
        ButtonsModule,
        DropdownModule,
        InputsModule,
        TableModule,
        CardsFreeModule,
        TooltipModule,
        ModalModule,
        CheckboxModule,
        BreadcrumbModule,
        IconsModule,
        InputUtilitiesModule
    ]
})
export class MDBModule { }
